OBJ_FILES = build.o calcb.o calcq.o calcz.o calerf.o io.o matrix.o noise.o tree.o weighbor.o

%.o: %.c
	gcc -c $^

all: $(OBJ_FILES)
	gcc -lm -o weighbor $^

clean: $(OBJ_FILES)
	rm -f $^
